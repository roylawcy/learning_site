from django import forms
from django.core import validators

def not_treehouse(value):
    treehouse = "@teamtreehouse.com"
    if value.lower().find(treehouse, len(treehouse)*-1):
        raise forms.ValidationError("not treehouse")

def must_be_empty(value):
    if value:
        raise forms.ValidationError('is not empty')

class SuggestionForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField(
        validators=[not_treehouse],)
    verify_email = forms.EmailField(help_text="Please verify your email address")
    suggestion = forms.CharField(widget=forms.Textarea)
    honeypot = forms.CharField(
        required=False,
        widget=forms.HiddenInput,
        label="Leave empty",
        validators=[must_be_empty],
        )

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get('email')
        verify = cleaned_data.get('verify_email')

        if email != verify:
            raise forms.ValidationError("You need to enter the same email in both fields")
    